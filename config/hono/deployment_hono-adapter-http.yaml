---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    client.lifecycle.config.k8s.io/mutation: ignore
    config.kubernetes.io/origin: |
      configuredIn: base/kustomization.yaml
      configuredBy:
        apiVersion: builtin
        kind: HelmChartInflationGenerator
  labels:
    app.kubernetes.io/component: adapter-http
    app.kubernetes.io/instance: hono
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: hono
    app.kubernetes.io/version: 2.1.1
    helm.sh/chart: hono-2.1.5
  name: hono-adapter-http
  namespace: hono
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/component: adapter-http
      app.kubernetes.io/instance: hono
      app.kubernetes.io/name: hono
  template:
    metadata:
      annotations:
        kubectl.kubernetes.io/default-container: adapter-http
        prometheus.io/path: /prometheus
        prometheus.io/port: "8088"
        prometheus.io/scheme: http
        prometheus.io/scrape: "true"
      labels:
        app.kubernetes.io/component: adapter-http
        app.kubernetes.io/instance: hono
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/name: hono
        app.kubernetes.io/version: 2.1.1
        helm.sh/chart: hono-2.1.5
      name: hono-adapter-http
      namespace: hono
    spec:
      containers:
      - env:
        - name: JDK_JAVA_OPTIONS
          value: -XX:MinRAMPercentage=80 -XX:MaxRAMPercentage=80
        - name: KUBERNETES_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: QUARKUS_CONFIG_LOCATIONS
          value: /opt/hono/default-logging-config/logging-quarkus-dev.yml
        image: index.docker.io/eclipse/hono-adapter-http:2.1.1
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /liveness
            port: health
            scheme: HTTP
          initialDelaySeconds: 300
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        name: adapter-http
        ports:
        - containerPort: 8088
          name: health
          protocol: TCP
        - containerPort: 8080
          name: http
          protocol: TCP
        - containerPort: 8443
          name: https
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /readiness
            port: health
            scheme: HTTP
          initialDelaySeconds: 20
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: "1"
            memory: 512Mi
          requests:
            cpu: 150m
            memory: 300Mi
        securityContext:
          privileged: false
        volumeMounts:
        - mountPath: /opt/hono/tls/tls.key
          name: tls-keys
          readOnly: true
          subPath: tls.key
        - mountPath: /opt/hono/tls/tls.crt
          name: tls-keys
          readOnly: true
          subPath: tls.crt
        - mountPath: /opt/hono/tls/ca.crt
          name: tls-trust-store
          readOnly: true
          subPath: ca.crt
        - mountPath: /opt/hono/default-logging-config
          name: default-logging-config
          readOnly: true
        - mountPath: /opt/hono/config
          name: adapter-http-conf
          readOnly: true
      volumes:
      - name: tls-keys
        secret:
          secretName: hono-adapter-http-example-keys
      - configMap:
          name: hono-example-trust-store
        name: tls-trust-store
      - configMap:
          name: hono-default-logging-config
          optional: true
        name: default-logging-config
      - name: adapter-http-conf
        secret:
          secretName: hono-adapter-http-conf
